﻿// Состав правок (исправлений)
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает набор доступных правок
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Массив
//
Функция Получить() Экспорт
	
	Результат	= Новый Массив();
	
	Результат.Добавить(ЗапускНаЛюбойПлатформе());
	Результат.Добавить(ОтсутствиеПрефиксаОтключениеПрефиксации());
	
	Возврат Результат;
	
КонецФункции // Получить 

// Возвращает описание правки ЗапускНаЛюбойПлатформе
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Структура
//
Функция ЗапускНаЛюбойПлатформе() Экспорт
	
	Результат	= испбсп_Описание.Создать();
	
	Текст		= "Необязательный. Позволяет запускать конфигурацию на 
	|любой версии платформы 1С Предприятие 8.3.17 и выше";
	
	испбсп_Описание.Идентификатор(Результат, испбсп_Идентификатор.ЗапускНаЛюбойПлатформе());
	испбсп_Описание.Вид(Результат, испбсп_Вид.Функционал());
	испбсп_Описание.Область(Результат, испбсп_Область.БазоваяФункциональность());
	испбсп_Описание.ВерсияМин(Результат, "3.1");
	испбсп_Описание.ВерсияМакс(Результат, "3.1");
	испбсп_Описание.Текст(Результат, Текст);
	испбсп_Описание.Методы(Результат, "ОМ.СтандартныеПодсистемыСервер.ПередЗапускомПрограммы");
	
	Возврат Результат;
	
КонецФункции // ЗапускНаЛюбойПлатформе 

// Возвращает описание правки ОтсутствиеПрефиксаОтключениеПрефиксации
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Структура
//
Функция ОтсутствиеПрефиксаОтключениеПрефиксации() Экспорт
	
	Результат	= испбсп_Описание.Создать();
	
	Текст		= "Отключает префиксацию для объектов в слечае отсутствия префикса и/или 
 	|единственного префикса ""00""";
	
	испбсп_Описание.Идентификатор(Результат, испбсп_Идентификатор.ОтсутствиеПрефиксаОтключениеПрефиксации());
	испбсп_Описание.Вид(Результат, испбсп_Вид.Функционал());
	испбсп_Описание.Область(Результат, испбсп_Область.ПрефиксацияОбъектов());
	испбсп_Описание.ВерсияМин(Результат, "3.1");
	испбсп_Описание.ВерсияМакс(Результат, "3.1");
	испбсп_Описание.Текст(Результат, Текст);
	испбсп_Описание.Методы(Результат, "ОМ.СтандартныеПодсистемыСервер.ПередЗапускомПрограммы");
	
	Возврат Результат;

КонецФункции // ОтсутствиеПрефиксаОтключениеПрефиксации 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 