﻿// Идентификаторы правок
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает идентификатор правки ЗапускНаЛюбойПлатформе
// 	Отключает проверку на минимальную минорную версию платформы при старте
// 	Устанавливает минимально допустимую версию платформы 8.3.17
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ЗапускНаЛюбойПлатформе() Экспорт
	
	Возврат "ЗапускНаЛюбойПлатформе";
	
КонецФункции // ЗапускНаЛюбойПлатформе 

// Возвращает идентификатор правки ОтсутствиеПрефиксаОтключениеПрефиксации
// 	Отключает префиксацию для объектов в слечае отсутствия префикса и/или 
// 	единственного префикса "00"
//
// Параметры: 
//
// Возвращаемое значение: 
// 	Строка
//
Функция ОтсутствиеПрефиксаОтключениеПрефиксации() Экспорт
	
	Возврат "ОтсутствиеПрефиксаОтключениеПрефиксации";
	
КонецФункции // ОтсутствиеПрефиксаОтключениеПрефиксации 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 